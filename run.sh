export AWS_ACCESS_KEY_ID="$WERCKER_EBDEPLOY_ACCESS_ID"
export AWS_SECRET_ACCESS_KEY="$WERCKER_EBDEPLOY_ACCESS_KEY"
mkdir -p "$WERCKER_SOURCE_DIR/.elasticbeanstalk/"
cat > $WERCKER_SOURCE_DIR/.elasticbeanstalk/config.yml << EOF
branch-defaults:
  $WERCKER_GIT_BRANCH:
    environment: $WERCKER_EBDEPLOY_ENVIRONMENT
global:
  application_name: $WERCKER_EBDEPLOY_APPLICATION
  default_region: $WERCKER_EBDEPLOY_REGION
  profile: null
  sc: git
EOF
exec 3>&1
for((i=0; i < 10; i++)); do
  out=$(eb deploy $WERCKER_EBDEPLOY_ENVIRONMENT --timeout $WERCKER_EBDEPLOY_TIMEOUT --nohang | tee >(cat - >&3))
  if [[ "$out" == *"invalid state"* ]]; then
	echo "Retrying in 10 seconds..."
	sleep 10
  else
	break
  fi
done
